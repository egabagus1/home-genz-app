<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Blank</title>

    <!-- Custom fonts for this template-->
    <link href="../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Plus+Jakarta+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="../assets/vendor/datatables/datatables.min.css" type="text/css">


    <!-- Custom styles for this template-->
    <link href="../assets/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <?php
    session_start();
    if ($_SESSION['status'] != "login") {
        header("location:../auth/index.php?pesan=belum_login");
    }
    ?>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include('../layout/sidebar.php'); ?>
        <!-- End of Sidebar -->


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <h4>List Data Barang</h4>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../index.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Barang</li>
                    </ol>
                    <div class="card shadow">
                        <div class="card-body">
                            <table class="table" id="tbl-barang">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Item Barang</th>
                                        <th>Stok</th>
                                        <th>Kategori</th>
                                        <th>Harga</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    <?php
                                    include '../koneksi.php';
                                    $no = 1;
                                    $fk_kategori = mysqli_query($koneksi, "SELECT * FROM tb_barang JOIN tb_kategori ON tb_barang.id_kategori = tb_kategori.id_kategori");
                                    while ($d = mysqli_fetch_array($fk_kategori)) {
                                    ?>
                                        <tr>
                                            <td><?php echo $d['id_barang']; ?></td>
                                            <td><?php echo $d['nama_barang']; ?></td>
                                            <td><?php echo $d['stok']; ?></td>
                                            <td><?php echo $d['nama_kategori']; ?></td>
                                            <td>Rp. <?php echo $d['harga']; ?></td>
                                            <td>
                                                <a class="btn btn-sm btn-primary" href="edit.php?id=<?php echo $d['id_barang']; ?>">EDIT</a>
                                                <a class="btn btn-sm btn-danger" href="delete.php?id=<?php echo $d['id_barang']; ?>">HAPUS</a>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>

                            </table>
                        </div>
                    </div>

                    <!-- Page Heading -->


                </div>
                <!-- /.container-fluid -->


            </div>
            <?php include('../layout/footer.php'); ?>
        </div>
        <!-- End of Content Wrapper -->



    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="../assets/vendor/jquery/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../assets/js/sb-admin-2.min.js"></script>

    <script src="../assets/vendor/datatables/datatables.min.js"></script>


    <script>
        let table = new DataTable('#tbl-barang');
    </script>
</body>

</html>