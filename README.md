Judul: Sistem Informasi Inventaris GenZ
Nama		: Ega Bagus Purnama

- SII (Sistem Informasi Inventaris GenZ) adalah website yang bisa digunakan untuk mengelola inventaris perusahaan.

- Teknologi yang digunakan adalah:
  -PHP dan mysql
  -Bootstrap
  -Jquery
  -DataTables

- Fitur dari SII GenZ antara lain:
  -Login dan logout
  -CRUD data barang
  -CRUD data kategori
  -CRUD data vendor
