<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Blank</title>

    <!-- Custom fonts for this template-->
    <link href="../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Plus+Jakarta+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="../assets/vendor/datatables/datatables.min.css" type="text/css">


    <!-- Custom styles for this template-->
    <link href="../assets/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <?php
    session_start();
    if ($_SESSION['status'] != "login") {
        header("location:../auth/index.php?pesan=belum_login");
    }
    ?>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include('../layout/sidebar.php'); ?>
        <!-- End of Sidebar -->


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <h4>Edit Data Kategori</h4>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="../kategori/index.php">Kategori</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit data</li>
                    </ol>
                    <div class="card shadow">
                        <div class="card-body">
                            <?php
                            include '../koneksi.php';
                            $id = $_GET['id'];
                            $data = mysqli_query($koneksi, "SELECT * FROM tb_kategori WHERE id_kategori='$id'");
                            while ($d = mysqli_fetch_array($data)) {
                            ?>
                                <form method="post" action="update.php">
                                    <div class="mb-3">
                                        <label class="form-label">ID Kategori</label>
                                        <input readonly type="text" class="form-control" name="id" value="<?php echo $d['id_kategori']; ?>">
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Nama Kategori</label>
                                        <input type="text" class="form-control" name="nama" value="<?php echo $d['nama_kategori']; ?>">
                                    </div>
                                    <input type="submit" value="Simpan" class="btn btn-md btn-primary">
                                </form>
                        </div>
                    </div>

                    <!-- Page Heading -->


                </div>
            <?php
                            }
            ?>
            <!-- /.container-fluid -->


            </div>
            <?php include('../layout/footer.php'); ?>
        </div>
        <!-- End of Content Wrapper -->



    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="../assets/vendor/jquery/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../assets/js/sb-admin-2.min.js"></script>

    <script src="../assets/vendor/datatables/datatables.min.js"></script>

    <script>
        let table = new DataTable('#tbl-kategori');
    </script>

</body>

</html>